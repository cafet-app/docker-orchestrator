#!/bin/bash

set -e

DIR=$(realpath "$(dirname "$(realpath "$0")")"/..)

source "$DIR"/config.sh

function login() {
    echo DGNvKHoDSYNLxxbZe2cr | docker login -u docker-token --password-stdin registry.gitlab.com
}

function get_name_from_path() {
    echo $1 | awk 'match($0, /\/([^\/]+)\/$/, r){print r[1]}'
}

function put_down_project() {
    echo "Sutting \"$1\" down.."
    sudo docker-compose -f "$DIR"/"$1"/docker-compose.yml down
    echo "\"$1\" is down."
}

function bring_up_project() {
    echo "Setting \"$1\" up..."
    sudo docker-compose -f $DIR/$1/docker-compose.yml pull
    sudo docker-compose -f $DIR/$1/docker-compose.yml up -d
    echo "\"$1\" is up and running"
}

function cert() {
    function generate() {
        sudo -E certbot --nginx --agree-tos -n -m $admin "${@/#/-d }"
    }

    function generate_all {
        mkdir -p "$DIR"/nginx/servers.d
        generate "$(grep server_name "$DIR"/nginx/servers.d/*.conf | awk 'match($0, /server_name (.*);$/,r){print r[1]}')"
    }

    function renew() {
        sudo -E certbot renew --agree-tos -n
    }

    function help() {
        printf "USAGE cafet-app-ctl cert COMMAND\n\n"
        printf "Manages SSL certificates.\n\n"
        printf "\tgenerate    DOMAINS...   generates SSL certificates for specified DOMMAINS if they are already registered\n"
        printf "\tgenrate-all              generates SSL certificates for every registered domains\n"
        printf "\trenew                    renews certificates if needed\n"
        printf "\thelp                     prints this message\n"
    }

    case $1 in
        generate)     generate "${@:2}";;
        generate-all) generate_all;;
        renew)        renew;;
        *)            help;;
    esac
}

function sharenet() {
    if ! docker network ls | grep cafet_sharenet; then
        sudo docker network create \
            --driver bridge \
            --gateway 172.100.5.254 \
            --ip-range 172.100.5.0/24 \
            --subnet 172.100.0.0/16 \
            cafet_sharenet
        echo Sharenet is now up and running
    else
        echo Sharenet is already configured
    fi
}

function up() {
    echo Testing sharenet
    sharenet

    login

    for i in `ls -d $DIR/*/ | grep -Ev 'sources|bin|nginx'`; do
        bring_up_project $(get_name_from_path $i)
    done
}

function down() {
    for i in `ls -d $DIR/*/ | grep -Ev 'sources|bin|nginx'`; do
        put_down_project $(get_name_from_path $i)
    done

    if [ -n "$(docker network ls | grep cafet_sharenet)" ]; then
        sudo docker network rm cafet_sharenet
        echo Sharenet has been put down
    else
        echo Sharenet is already down
    fi
}

function restart() {
  down
  up
}

function new_project() {
    if [[ "$1" = "" || "$1" = "--help" || "$1" = "help" ]]; then
        echo "USAGE: cafet-app-cli new-project PROJECT_NAME DOMAINS..."
        return 1;
    fi

    project=${1/\//_}
    ip=$(($(docker network inspect cafet_sharenet | grep IPv4Address | awk 'BEGIN{max=0} match($0, /[0-9]+\.[0-9]+\.[0-9]\.([0-9])\//, r){if(r[1]>max)  max=r[1]} END{print max}') + 1))

    sharenet

    mkdir "$project"
    sed "s/servername/$project/g" "$DIR/docker-compose.yml" | sed s/172.100.0.0/172.100.0.$ip/g >"$DIR/$project/docker-compose.yml"
    bring_up_project $project

    mkdir -p $DIR/nginx/servers.d
    echo "server {
    listen 80;
    listen [::]:80;

    server_name ${*:2};

    location / {
        proxy_pass http://172.100.0.$ip;
    }
}
" > $DIR/nginx/servers.d/$project.conf
    sudo nginx -t
    sudo nginx -s reload

    cert generate "${@:2}"
}

function remove() {
    if [[ "$1" = "" || "$1" = "--help" || "$1" = "help" ]]; then
        echo "USAGE: cafet-app-cli remove PROJECT_NAME "
        return 1;
    fi

    put_down_project $1

    mkdir -p $DIR/nginx/servers.d
    for certificate in $(grep server_name $DIR/nginx/servers.d/$1.conf | awk '{$1="";$2="";print $0;}'); do
        sudo certbot delete --cert-name "$certificate"
    done

    rm -v "$DIR/nginx/servers.d/$1.conf"
    sudo nginx -t
    sudo nginx -s reload

    rm -rvd "${DIR:?}/$1"
}

function help() {
    printf "USAGE: cafet-app-ctl COMMAND\n\n"
    printf "Interact with cafet-app servers\n\n"
    printf "\tcert                  manges SSL certificates\n"
    printf "\tsharenet              brings sharenet up if not already (network that connects application containers to host)\n"
    printf "\trestart               trestart all services and containers up\n"
    printf "\tup                    puts all services and containers up\n"
    printf "\tdown                  kills all services and containers\n"
    printf "\tnew    NAME DOMAINS   creats a new server\n"
    printf "\tremove NAME           remove a server (cannot be undone)\n"
    printf "\tupdate                update instances\n"
    printf "\thelp                  prints this message\n"
}

case $1 in
    cert)     cert "${@:2}";;
    sharenet) sharenet;;
    restart)  restart;;
    up)       up;;
    down)     down;;
    new)      new_project "${@:2}";;
    remove)   remove "${@:2}";;
    *)        help;;
esac
